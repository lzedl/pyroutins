__all__ = ["RawSystemCommand", "Sed", "Curl", "Awk", "Cat", "Cut", "Sort", "Uniq", "Join", "Grep", "Echo", "Sed", "Nl",
           "NumberLines", "Mysql", "MysqlImport",
           "PythonRawFunction", "PythonMapFunction", "PythonReduceFunction", "routine", "changeSubProcessesLocale"]

import multiprocessing
import subprocess
import sys
import os


ENVIRONMENT = None


def changeSubProcessesLocale(locale="C"):
    global ENVIRONMENT
    ENVIRONMENT = os.environ.copy()
    ENVIRONMENT["LC_ALL"] = locale
    ENVIRONMENT["LANG"] = locale


class TextConnectionsWrapper(object):
    def __init__(self, connection):
        self.conn = connection

    def __iter__(self):
        return self

    def next(self):
        try:
            return self.conn.recv_bytes()
        except EOFError:
            raise StopIteration

    def write(self, string):
        self.conn.send_bytes(string)

    def readline(self):
        return self.conn.recv_bytes()

    def readlines(self):
        return [x for x in self]

    def writelines(self, lines):
        for line in lines:
            self.write(line)

    def close(self):
        self.conn.close()


def TextPipe():
    end, start = multiprocessing.Pipe(False)
    startWrapper = TextConnectionsWrapper(start)
    endWrapper = TextConnectionsWrapper(end)

    return startWrapper, endWrapper


class CommandBase(object):
    def __init__(self, command, *args):
        self.command = command
        self.arguments = list(args)
        self.process = None
        self.previous = None
        self.next = None

    def __str__(self):
        return "%s %s" % (self.command, " ".join([x for x in self.arguments]))

    def addArg(self, arg):
        self.arguments.append(arg)

    def addArgs(self, *args):
        for arg in args:
            self.addArg(arg)

    def getContent(self):
        return str(self)

    def getProcessStdin(self):
        if self.process is None:
            self.process = self.toProcess()
        return self.process.stdin

    def getProcessStdout(self):
        if self.process is None:
            self.process = self.toProcess()
        return self.process.stdout

    def toProcess(self):
        if self.process is None:
            #============================
            if isinstance(self.previous, CommandBase):
                stdin = self.previous.getProcessStdout()
            elif self.previous is None:
                stdin = None
            else:
                stdin = subprocess.PIPE
            #============================
            if self.next is None:
                stdout = None
            elif isinstance(self.next, (CommandBase, PythonFunctionBase)):
                stdout = subprocess.PIPE
            else:
                stdout = self.next.fileno()
            #============================
            # print "build process for", type(self), "$(%s)" % str(self)
            # print "args: ", self.arguments
            # print "previous:", type(self.previous)
            # print "next:", type(self.next)
            # print "stdin:", type(stdin), stdin
            # print "stdout:", type(stdout), stdin
            # print "=" * 30
            self.process = subprocess.Popen(
                [self.command] + self.arguments,
                stdin=stdin,
                stdout=stdout,
                close_fds=True,
                env=ENVIRONMENT
            )
        return self.process

    def run(self, output=None, append=False):
        if output:
            output = open(output, "wb" if not append else "ab")
        self.previous = None
        self.next = output
        process = self.toProcess()
        process.wait()
        if output:
            output.close()


class RawSystemCommand(CommandBase):
    def __init__(self, command, *args):
        CommandBase.__init__(self, command, *args)


class PythonFunctionBase(object):
    def __init__(self, processClass, func, processParams=None):
        self.processClass = processClass
        self.func = func
        self.process = None
        self.previous = None
        self.next = None
        self.processParams = processParams or {}

    def getProcessStdin(self):
        if self.process is None:
            self.process = self.toProcess()
        return self.stdin

    def getProcessStdout(self):
        if self.process is None:
            self.process = self.toProcess()
        return self.process.stdout

    def toProcess(self):
        if self.process is None:
            #============================
            if isinstance(self.previous, CommandBase):
                stdin = self.previous.getProcessStdout()
            elif isinstance(self.previous, PythonFunctionBase):
                r, w = os.pipe()
                stdin = os.fdopen(r)
                self.stdin = os.fdopen(w, "w")
            else:
                r, w = os.pipe()
                stdin = os.fdopen(r)
                self.stdin = os.fdopen(w, "w")
                self.stdin.close()
            #============================
            if isinstance(self.next, (CommandBase, PythonFunctionBase)):
                stdout = self.next.getProcessStdin()
            elif self.next is None:
                stdout = sys.stdout
            else:
                stdout = self.next
            #============================
            # print "build process for", type(self)
            # print "previous:", type(self.previous)
            # print "next:", type(self.next)
            # print "stdin:", type(stdin)
            # print "stdout:", type(stdout)
            # print "=" * 30
            return self.processClass(self.func, stdin, stdout, **self.processParams)
        return self.process


class PythonFunctionProcessBase(multiprocessing.Process):
    def __init__(self, func, stdin, stdout):
        multiprocessing.Process()
        self.func = func
        self.stdout = stdout
        self.stdin = stdin

    def wait(self):
        self.proceed(self.func, self.stdin, self.stdout)
        # if isinstance(self.stdin, BUFFER_CLASS):
        #     self.stdin.close()
        # if not isinstance(self.stdout, BUFFER_CLASS):
        #     self.stdout.close()
        # else:
        #     self.stdout.flush()
        self.stdin.close()
        self.stdout.close()

    def proceed(self, func, stdin, stdout):
        """Reimplement me"""


class Cat(CommandBase):
    def __init__(self, filename):
        if not isinstance(filename, (list, tuple)):
            filename = [filename]
        CommandBase.__init__(self, "cat", *filename)


class Cut(CommandBase):
    def __init__(self, fields, delimiter="\t"):
        CommandBase.__init__(self, "cut", "-d", delimiter, "-f", ",".join([str(x) for x in fields]))


class Sort(CommandBase):
    def __init__(self, fields=None, reverse=False, delimiter="\t"):
        CommandBase.__init__(self, "sort", "-t", delimiter)
        if fields:
            for f in fields:
                self.addArg("-k%d" % f)
        if reverse:
            self.addArg("--reverse")


class Uniq(CommandBase):
    def __init__(self, count=False, repeated=False, unique=False):
        CommandBase.__init__(self, "uniq")
        if count:
            self.addArg("--count")
        if repeated:
            self.addArg("--repeated")
        if unique:
            self.addArg("--unique")


class Join(CommandBase):
    def __init__(self, filename1, filename2, field1, field2, delimiter="\t",
                 unprintable=None, outputFormat=None, empty=None, header=False):
        CommandBase.__init__(
            self,
            "join",
            filename1,
            filename2,
            "-1", "%d" % field1,
            "-2", "%d" % field2,
            "-t", "%s" % delimiter
        )
        if unprintable:
            self.addArgs("-a", str(unprintable))
        if outputFormat:
            if isinstance(outputFormat, (tuple, list)):
                outputFormat = ",".join(outputFormat)
            self.addArgs("-o", outputFormat)
        if empty:
            self.addArgs("-e", empty)
        if header:
            self.addArg("--header")


class Grep(CommandBase):
    def __init__(self, pattern, caseInsensitive=False, invertMath=False, onlyMatching=False):
        CommandBase.__init__(self, "grep", "%s" % pattern)
        if caseInsensitive:
            self.addArg("-i")
        if invertMath:
            self.addArg("-v")
        if onlyMatching:
            self.addArg("-o")


class Echo(CommandBase):
    def __init__(self, string):
        CommandBase.__init__(self, "echo", "%s" % string)


class Sed(CommandBase):
    def __init__(self, pattern):
        CommandBase.__init__(self, "sed", pattern)


class Curl(CommandBase):
    def __init__(self, url, silent=True):
        CommandBase.__init__(self, "/usr/bin/curl", url)
        if silent:
            self.addArg("--silent")


class Awk(CommandBase):
    def __init__(self, begin="", main="", end="", delimiter="\t", outputDelimiter="\t"):
        CommandBase.__init__(self, "awk", "-F", delimiter, "-v", "OFS=%s" % outputDelimiter)
        script = ""
        if begin:
            script = "BEGIN { %s } " % begin
        if main:
            script += "{ %s } " % main
        if end:
            script += "END { %s }" % end
        self.addArg(script)


class Nl(CommandBase):
    def __init__(self, width=1):
        CommandBase.__init__(self, "nl", "-w", str(width))


NumberLines = Nl


class Mysql(CommandBase):
    def __init__(self, query, quick=True, defaultsExtraFile=None, defaultCharacterSet="utf8", header=True):
        CommandBase.__init__(self, "mysql")
        if defaultsExtraFile:
            self.addArg("--defaults-extra-file=%s" % defaultsExtraFile)
        if quick:
            self.addArg("--quick")
        if defaultCharacterSet:
            self.addArg("--default-character-set=%s" % defaultCharacterSet)
        self.addArg("--column-names" if header else "--disable-column-names")
        self.addArgs("-e", query)
        # print " ".join(self.arguments)


class MysqlImport(Mysql):
    def __init__(self, filename, table, fields, delete=False, defaultsExtraFile=None, defaultCharacterSet="utf8"):
        sql = """SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE; SET autocommit=0; START TRANSACTION;
        {delete}
        LOAD DATA LOCAL INFILE '{filename}' IGNORE INTO TABLE {table} ({fields});
        SHOW WARNINGS;
        COMMIT;""".format(
            filename=filename,
            table=table,
            fields=", ".join(fields),
            delete="DELETE FROM %s;" % table if delete else ""
        )
        Mysql.__init__(self, sql, True, defaultsExtraFile, defaultCharacterSet)
        self.addArg("--local-infile=1")
        self.addArg("--silent")


class PythonRawFunctionProcess(PythonFunctionProcessBase):
    def __init__(self, func, stdin, stdout):
        PythonFunctionProcessBase.__init__(self, func, stdin, stdout)

    def proceed(self, func, stdin, stdout):
        func(stdin, stdout)


class PythonMapFunctionProcess(PythonFunctionProcessBase):
    def __init__(self, func, stdin, stdout):
        PythonFunctionProcessBase.__init__(self, func, stdin, stdout)

    def proceed(self, func, stdin, stdout):
        for line in stdin:
            for outLine in func(line):
                stdout.write(outLine)


class PythonReduceFunctionProcess(PythonFunctionProcessBase):
    def __init__(self, func, stdin, stdout, delimiter="\t", keys=None, outputDelimiter=None):
        PythonFunctionProcessBase.__init__(self, func, stdin, stdout)
        self.delimiter = delimiter
        self.keys = keys or [0]
        self.outputDelimiter = self.delimiter if outputDelimiter is None else outputDelimiter

    def proceed(self, func, stdin, stdout):
        """
        Here we need function like someReduceFunc(key -> list of strings (fields), lines -> list of strings)
        """
        def _reduce(key, lines):
            for outLine in func(key, lines):
                stdout.write(outLine)
        #FIXME
        line = next(stdin).strip()
        values = line.split(self.delimiter)
        currentKey = [x for n, x in enumerate(values) if n in self.keys]
        lines = [line]
        for line in stdin:
            line = line.strip()
            values = line.split(self.delimiter)
            key = [x for n, x in enumerate(values) if n in self.keys]
            if currentKey != key:
                _reduce(currentKey, lines)
                currentKey = key
                lines = []

            lines.append(line)

        _reduce(currentKey, lines)


class PythonRawFunction(PythonFunctionBase):
    def __init__(self, func):
        PythonFunctionBase.__init__(self, PythonRawFunctionProcess, func)


class PythonMapFunction(PythonFunctionBase):
    def __init__(self, func):
        PythonFunctionBase.__init__(self, PythonMapFunctionProcess, func)


class PythonReduceFunction(PythonFunctionBase):
    def __init__(self, func, keys, delimiter="\t", outputDelimiter=None):
        PythonFunctionBase.__init__(
            self,
            PythonReduceFunctionProcess,
            func,
            processParams={
                "delimiter": delimiter,
                "outputDelimiter": outputDelimiter,
                "keys": keys
            }
        )


def routine(commands, output=None, append=False):
    if output:
        output = open(output, "wb" if not append else "ab")
    processes = []

    previous = None
    for n, command in enumerate(commands):
        if n == len(commands) - 1:
            nextCommand = output
        else:
            nextCommand = commands[n + 1]

        command.previous = previous
        command.next = nextCommand

        previous = command

    for command in commands:
        processes.append(command.toProcess())

    for n, process in enumerate(processes):
        if not isinstance(process, subprocess.Popen) or n == len(processes) - 1:
            process.wait()

    if output:
        output.close()
